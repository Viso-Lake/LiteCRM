# LiteCRM - система приема заказов службы доставки еды

### Изображения с примером работы и оформлением заказов.

* Пустая форма
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/empty-order.png"
  alt="Пустая форма"
  width="900">

* Заполненная форма
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/new-order.jpg"
  alt="Заполненная форма"
  width="900">

* Печать накладной
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/print-invoice.jpg"
  alt="Печать накладной"
  width="500">

* Распечатанная накладная А4 формата
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/invoice.jpg"
  alt="Распечатанная накладная А4 формата"
  width="500">

* Печать чека
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/print-check.jpg"
  alt="Печать чека"
  width="250">

* Распечатанный чек 58мм (ширина рулона)
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/check.jpg"
  alt="Распечатанный чек 58мм (ширина рулона)"
  width="250">

* Список заказов в работе
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/orders-list.jpg"
  alt="Список заказов в работе"
  width="900">

* Список предзаказов
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/preorder-list.png"
  alt="Список предзаказов"
  width="900">

* Список выполненных/отмененных заказов
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/complete-order.jpg"
  alt="Список выполненных/отмененных заказов"
  width="900">
  
* Дашборд
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/dash.png"
  alt="Дашборд"
  width="900">

* Форма авторизации
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/auth.png"
  alt="Форма авторизации"
  width="400">

* Блок-схема обработки заказа по номеру тел
  * <img
  src="https://github.com/Coffe-Lake/LiteCRM/blob/master/static/image/exmpl_imgs_crm/scheme.png"
  alt="Блок-схема обработки заказа по номеру тел"
  width="700">
    
